// IconFactory.cpp : 定义控制台应用程序的入口点。
//

#include <SDKDDKVer.h>
#include <stdio.h>
#include <tchar.h>
#include "_cpp.h"
#include "_string.h"
#include "efolder.h"
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;
static const std::vector<int> s_whs = { 28, 40, 48, 58, 60, 72, 80, 87, 96, 108, 120, 144, 180, 192, 256, 384, 512 };
static const string s_export = "导出/";
static string s_name, s_suffix = ".png";
#define ccc_md(_path_)system(("md " + cc::replace((_path_), "/", "\\")).c_str())
#define ccc_move(_name_, _path_move_)system(("move " + cc::replace(((_name_) + " " + (_path_move_)), "/", "\\")).c_str())
#define ccc_copy(_name_, _name_copy_)system(("copy " + cc::replace(((_name_) + " " + (_name_copy_)), "/", "\\")).c_str())

bool setPzb(string name)
{
	ofstream ofile("PhotoZoomPro3.Pzb");
	if(!ofile)
	{
		return false;
	}
	ofile << "Version=1.1";
	ofile << "\n";
	ofile << "PathFormat=win";
	ofile << "\n";
	ofile << "[Items]";
	ofile << "\n";
	forv(s_whs, k)
	{
		ofile << toString("[Items/Item%d]", k);
		ofile << "\n";
		ofile << toString("Cmd=-i \"%s\" -w %dpixels -m ssplinemax -s \"-nousm -in 1 -ra 2.5 -fg 20\"",name.c_str(), s_whs[k]);
		ofile << "\n";
	}
	ofile << "[Settings]";
	ofile << "\n";
	ccc_md(s_export);
	ofile << toString("Output\\ Folder=%s\\\\",s_export.substr(0,s_export.length() - 1).c_str());
	ofile << "\n";
	ofile << "Create\\ Subfolders=0";
	ofile << "\n";
	ofile << "Output\\ Format=png";
	ofile << "\n";
	ofile << "File\\ Exists\\ Action=Unique Name";
	ofile.close();
	return true;
}

bool step0()
{
	const auto& names = cc::efolder("", true, 0);
	s_name = "";
	bool hadPhotoZoom = false;
	for(const auto& name :names)
	{
		if(s_name.empty() && name.find(".png") != std::string::npos)
		{
			printf("导入 %s\n", name.c_str());
			s_name = name;
		}
		if(!hadPhotoZoom && name == "PhotoZoomPro3.exe")
		{
			hadPhotoZoom = true;
		}
	}
	if(s_name.empty())
	{
		printf("找不到App图标\n");
	}
	return hadPhotoZoom;
}

bool step1()
{
	const auto& names = cc::efolder(s_export, true, 0);
	if(names.size() > 0)
	{
		printf("请先清空<导出>文件夹\n");
		return false;
	}
	bool hadPhotoZoom = step0();
	if(s_name.empty())
	{
		printf("找不到App图标\n");
		return false;
	}
	if(!hadPhotoZoom){
		printf("找不到PhotoZoomPro3.exe\n");
		return false;
	}
	if(!setPzb(s_name))
	{		
		printf("无法导出PhotoZoomPro3.pzb\n");
		return false;
	}
	printf("导出PhotoZoomPro3.pzb\n请打开PhotoZoomPro3.exe\n-->批量处理-->打开-->导入批处理 PhotoZoomPro3.pzb\n-->全选-->运行-->开始批量处理\n");
	system("PhotoZoomPro3");
	return true;
}


bool step2()
{
	step0();
	if(s_name.empty())
	{
		printf("找不到App图标\n");
		return false;
	}
	string subname = s_name.substr(0, s_name.length() - s_suffix.length());
	vector<int> vs;
	set<int> ss;
	const auto& names = cc::efolder(s_export, true, 0);
	for(auto name :names)
	{
		if(name == s_name)
		{
			vs.push_back(0);
			ss.insert(0);
			rename((s_export + name).c_str(), (s_export + subname + "0" + s_suffix).c_str());
		}
		else
		{
			if(name.find(subname) != string::npos)
			{
				name = name.substr(subname.length());
				vs.push_back(::atoi(name.c_str()));
				ss.insert(::atoi(name.c_str()));
			}
		}
	}
	if(vs.size() != ss.size() || vs.size() != s_whs.size())
	{
		printf("App图标尺寸不全\n");
		return false;
	}
	// { 28, 40, 48, 58, 60, 72, 80, 87, 96, 108, 120, 144, 180, 192, 256, 384, 512 };
	std::set<int> ins = { 11, 13, 14, 15, 16 };

	vs.clear();
	ss.clear();
	forv(s_whs, k)
	{
		if (ins.find(k) == ins.end())
		{
			ss.insert(k);
		}
		else
		{
			vs.push_back(k);
		}
	}
	vector<string> fs;
	fs.push_back("mipmap-mdpi/");
	fs.push_back("mipmap-hdpi/");
	fs.push_back("mipmap-xhdpi/");
	fs.push_back("mipmap-xxhdpi/");
	fs.push_back("mipmap-xxxhdpi/");
	string sub1, sub2;
	forr(vs, k)
	{
		sub1 = s_export + subname + cc::toString(vs[k]) + s_suffix;
		sub2 = s_export + s_name; // "ic_launcher.png";
		rename(sub1.c_str(), sub2.c_str());
		ccc_md(s_export + fs[k]);
		ccc_move(sub2, s_export + fs[k]);
	}
	for(const auto& s : ss)
	{
		sub1 = s_export + subname;
		sub2 = sub1;
		sub1 += cc::toString(s) + s_suffix;
		sub2 += cc::toString(s_whs[s]) + s_suffix;
		rename(sub1.c_str(), sub2.c_str());
	}
	printf("App图标重命名完成\n");
	return true;
}


int _tmain(int argc, _TCHAR* argv[])
{
	printf("App图标for Android & IOS\nMade in Chine by 挨踢狼\n为确保程序正常工作,请以管理员身份运行");
	int input = -1;
	while (true)
	{
		printf("请输入以下选项:\n0.注册码\n1.导出各个尺寸的App图标\n2.导出图标后进行重命名\n");
		cin >> input;
		if(input == 0)
		{
			ofstream ofile("PhotoZoomPro3.txt");
			ofile << "BBMKQXN5S8MFTGX2CKFQZM5PC";
			ofile.close();
			printf("导出PhotoZoomPro3.txt\n");
		}
		else if(input == 1)
		{
			step1();
		}
		else if(input == 2)
		{
			step2();
		}
		system("pause");
	}
	return 0;
}

